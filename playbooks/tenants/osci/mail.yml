---

- hosts: mail_servers
  roles:
  - role: postgrey
    whitelist_clients: "{{ cage_subnets }}"
  - role: spamassassin
    service_profile: low
  tags: antispam

- hosts: mail_servers
  tasks:
    - name: "Install MTA-STS resolver for postfix"
      include_role:
        name: mta_sts_resolver
      vars:
        cache_type: sqlite
        debug: True
  tags:
    - mta-sts
    - mta_sts_resolver

- hosts: mail_servers
  tasks:
    - name: "Generate SMTP certificates"
      include_role:
        name: letsencrypt_rfc2136
        tasks_from: gen_cert
      vars:
        mail_domain: osci.io
        domain: "{{ mx.dns_name }}"
  tags: mta_cert

# MTA MX1
- hosts: mail_servers_mx1
  tasks:
    - name: "Tenants hosted mail domains: compute parameters"
      include_tasks: mail_tenant_pre.yml
      with_dict: "{{ tenant_hosted_mail_domains }}"
      loop_control:
        loop_var: tenant
    - name: Install MTA
      include_role:
        name: postfix
      vars:
        myhostname: "{{ mx.dns_name }}"
        mydestination: "{{ mx_domains_int }}"
        relay_domains: "{{ mx_domains_ext }}"
        mynetworks: "{{ cage_subnets }}"
        auth: sasldb
        with_postgrey: true
        with_spamassassin: true
        with_postsrsd: true
        smtpd_options:
          content_filter: spamfilter
        local_accounts:
          # tenant: osci
          - ticket
          - ticket-comment
          - ticket-rdo-cloud
          - ticket-rdo-cloud-comment
          # tenant: gdb
          - gnutoolchain-gerrit
          # tenant: pulp
          - pulp-discourse
        aliases:
          root: "{{ ['root'] + comminfra_tech_emails }}"
          contact: root
          security: root
          infra: root
          hostmaster: root
          webmaster: root
          openshift-admin: root
          no-reply: /dev/null
          comminfra: "{{ comminfra_team_emails }}"
          jetpack-admin: "{{ jetpack_admins_emails }}"
        tls_method: manual
        cert_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/fullchain.pem"
        key_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/privkey.pem"
        with_mta_sts: True
    - name: Create smtp users
      include_role:
        name: sasldb
      vars:
        db_group: postfix
        user_create:
          - "{{ discourse_os_smtp_user }}"
    - name: "Tenants hosted mail domains: install assets"
      include_tasks: mail_tenant_post.yml
      with_dict: "{{ tenant_hosted_mail_domains }}"
      loop_control:
        loop_var: tenant
  tags: mta

# MDA
- hosts: mail_servers_mx1
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/osci/mail/"
  tasks:
    - name: "Generate SMTP certificates"
      include_role:
        name: letsencrypt_rfc2136
        tasks_from: gen_cert
      vars:
        mail_domain: osci.io
        domain: "{{ osci_mda }}"

    - name: "Install Dovecot"
      include_role:
        name: dovecot
      vars:
        auth: yaml-dict
        cert_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/cert.pem"
        key_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/privkey.pem"
        ca_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/chain.pem"
        # used by discourse
        with_pop3: True

    - name: install Dovecot users
      copy:
        src: "{{ data_dir }}/users/"
        dest: /etc/dovecot/users/
        owner: root
        group: dovecot-proxy
        mode: 0640

    # LE hooks order:
    #   - 03: deploy cert if needed (copy and change perms for non-root services)
    #   - 07: restart/reload/systemctl kill/… to notify the service to take the new cert into account
    # other levels for special actions
    - name: "Install Let's Encrypt renewal hook"
      template:
        src: "{{ data_dir }}/07_dovecot_cert_renewal_restart_service"
        dest: /etc/letsencrypt/renewal-hooks/deploy/
        owner: root
        group: root
        mode: 0755
  tags: mda

# MTA MX2
- hosts: mail_servers_mx2
  tasks:
    - name: Install MTA
      include_role:
        name: postfix
      vars:
        myhostname: "{{ mx.dns_name }}"
        mydestination: []
        relay_domains: "{{ mx_domains_int | union(mx_domains_ext) | union(tenant_hosted_mail_domains.values() | sum(start = [])) }}"
        mynetworks: "{{ cage_subnets }}"
        with_postgrey: true
        with_spamassassin: true
        smtpd_options:
          content_filter: spamfilter
        tls_method: manual
        cert_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/fullchain.pem"
        key_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/privkey.pem"
        with_mta_sts: True
  tags: mta

- hosts: www.osci.io
  vars:
    domain: osci.io
    mx1_dns_names: "{{ groups['dns_servers_ns1'] | map('extract', hostvars) | map(attribute='mx') | map(attribute='dns_name') | list }}"
    mx2_dns_names: "{{ groups['dns_servers_ns2'] | map('extract', hostvars) | map(attribute='mx') | map(attribute='dns_name') | list }}"
  tasks:
    - name: "Install MTA-STS for OSCI domain"
      include_role:
        name: mta_sts_policy
      vars:
        document_root: "/var/www/{{ website_domain }}"
        mx_list: "{{ mx1_dns_names + mx2_dns_names }}"
        dns_host: polly.osci.io
        dns_file: "/etc/named/masters/{{ domain }}.mta-sts"
  tags:
    - mta_sts
    - mta_sts_policy

