---
# Group assignation for hosts:
# - tenant group (necessary, only one)
# - 'unmanaged' group, if provisonned by us but managed by the tenant
# - if not unmanaged:
#   + service groups
#   + specific groups
#
all:
  vars:
    ansible_user: root
    kakunin_os_image: "centos/7"

  children:
    tenant_osci:
      children:
        osci_okd:
          vars:
            osci_monitoring: False
            osci_access: False
          hosts:
            lb.okd.osci.io:
              osci_access: True
            master-0.okd.osci.io:
            master-1.okd.osci.io:
            master-2.okd.osci.io:
            worker-1.okd.osci.io:
            worker-2.okd.osci.io:
      hosts:
        polly.osci.io:
          ansible_host: 8.43.85.229
          ansible_python_interpreter: /usr/bin/python3
        francine.osci.io:
          ansible_host: 8.43.85.230
          ansible_python_interpreter: /usr/bin/python3
        osci-web-builder.int.osci.io:
          ansible_host: 172.24.32.11
          ansible_python_interpreter: /usr/bin/python3
        www.osci.io:
          ansible_host: 8.43.85.237
        piwik-vm.osci.io:
          ansible_host: 8.43.85.206
          ansible_python_interpreter: /usr/bin/python3
        soeru.osci.io:
          ansible_host: 8.43.85.211
          ansible_python_interpreter: /usr/bin/python3
        lucille.srv.osci.io:
        catton.osci.io:
          ansible_python_interpreter: /usr/bin/python3
        carla.osci.io:
          # don't change DNS settings
          playbook_test_mode: True
          ansible_python_interpreter: /usr/bin/python3
        cauldron.osci.io:
          ansible_python_interpreter: /usr/bin/python3
        augur.osci.io:
          ansible_python_interpreter: /usr/libexec/platform-python3.6
        sandiego.osci.io:
          ansible_python_interpreter: /usr/libexec/platform-python3.6
        bigaldente.srv.osci.io:
          ansible_python_interpreter: /usr/bin/python3
        spritz.osci.io:
          ansible_python_interpreter: /usr/bin/python3
        jupyter.osci.io:
          ansible_python_interpreter: /usr/bin/python3
          ansible_host: 8.43.85.243
        gl-runner-podman-01.osci.io:
          ansible_python_interpreter: /usr/bin/python3
          ansible_host: 8.43.85.127
        badcat.osci.io:
          ansible_python_interpreter: /usr/bin/python3


    tenant_beaker:
      hosts:
        beaker-project.osci.io:
          ansible_host: 8.43.85.221

    tenant_dogtagpki:
      hosts:
        lists.dogtagpki.org:
          ansible_host: 8.43.85.251

    tenant_gdb:
      hosts:
        gdb-buildbot.osci.io:
          ansible_host: 8.43.85.197
        gnutoolchain-gerrit.osci.io:
          ansible_host: 8.43.85.239

    tenant_gnocchi:
      hosts:
        gnocchi.osci.io:
          ansible_host: 8.43.85.232

    tenant_heptapod:
      hosts:
        heptapod.osci.io:
          ansible_host: 8.43.85.240

    tenant_jboss:
      hosts:
        lists.jboss.org:
          ansible_host: 8.43.85.220

    tenant_koji:
      hosts:
        www.koji.build:
          ansible_host: 8.43.85.249
        koji-web-builder.int.osci.io:
          ansible_host: 172.24.32.33

    tenant_minishift:
      hosts:
        lists.minishift.io:
          ansible_host: 8.43.85.234

    tenant_enterpriseneurosystem:
      hosts:
        lists.enterpriseneurosystem.org:
          ansible_host: 8.43.85.242

    tenant_nfs_ganesha:
      hosts:
        lists.nfs-ganesha.org:
          ansible_host: 8.43.85.212

    tenant_opendatahub:
      hosts:
        lists.opendatahub.io:
          ansible_host: 8.43.85.215

    tenant_openjdk:
      hosts:
        openjdk-sources.osci.io:
          ansible_host: 8.43.85.238

    tenant_opensourceinfra:
      hosts:
        lists.opensourceinfra.org:
          ansible_host: 8.43.85.233

    tenant_ovirt:
      hosts:
        ovirt-web-builder.int.osci.io:
          ansible_host: 172.24.32.15
        mail.ovirt.org:
          ansible_host: 8.43.85.194
          osci_monitoring: True
        monitoring.ovirt.org:
          ansible_host: 8.43.85.199
        www.ovirt.org:
          ansible_host: 8.43.85.224
        glance.ovirt.org:
          ansible_host: 8.43.85.218

    tenant_pcp:
      hosts:
        pcp.osci.io:
          ansible_host: 8.43.85.210

    tenant_po4a:
      hosts:
        www.po4a.org:
          ansible_host: 8.43.85.209
        lists.po4a.org:
          ansible_host: 8.43.85.228

    tenant_podman:
      hosts:
        lists.podman.io:
          ansible_host: 8.43.85.227

    tenant_pulp:
      hosts:
        www.pulpproject.org:
          ansible_host: 8.43.85.236
        pulp-web-builder.int.osci.io:
          ansible_host: 172.24.32.12
          ansible_python_interpreter: /usr/bin/python3

    tenant_rdo:
      hosts:
        rdo-web-builder.int.osci.io:
          ansible_host: 172.24.32.16
        lists.rdoproject.org:
          ansible_host: 8.43.85.121

    tenant_spice:
      hosts:
        spice-web.osci.io:
          ansible_host: 8.43.85.198

    tenant_theopensourceway:
      hosts:
        tosw-ng-2019.theopensourceway.org:
          ansible_host: 8.43.85.213
        osci.theopensourceway.org:
          ansible_host: 8.43.85.193

    tenant_atomic:
      vars:
        osci_access: False
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:

    tenant_patternfly:
      hosts:
        patternfly-forum.osci.io:
          ansible_host: 8.43.85.214
          ansible_python_interpreter: /usr/bin/python3
          osci_access: False

    tenant_python:
      vars:
        osci_access: False
      hosts:
        python-builder-rawhide.osci.io:
          ansible_host: 8.43.85.216
          ansible_python_interpreter: /usr/bin/python3
        python-builder2-rawhide.osci.io:
          ansible_host: 8.43.85.222
          ansible_python_interpreter: /usr/bin/python3
        python-builder-rhel7.osci.io:
          ansible_host: 8.43.85.223
        python-builder-rhel8.osci.io:
          ansible_host: 8.43.85.235
        python-builder-rhel8-fips.osci.io:
          ansible_host: 8.43.85.241
        python-builder-c9s.osci.io:
          ansible_host: 8.43.85.125
        python-builder-c9s-fips.osci.io:
          ansible_host: 8.43.85.126

    tenant_zanata:
      hosts:
        translate.zanata.org:
          ansible_host: 8.43.85.219

    tenant_chrisproject:
      hosts:
        lists.chrisproject.org:
          ansible_host: 8.43.85.244

    tenant_shipwright:
      hosts:
        lists.shipwright.io:
          ansible_host: 8.43.85.195

    tenant_osbusiness:
      hosts:
        lists.osbusiness.org:
          ansible_host: 8.43.85.246

    tenant_ceph:
      hosts:
        nextcloud-ceph.osci.io:
          ansible_host: 8.43.85.247
      children:
        ceph_openshift_tests:
          vars:
            osci_monitoring: False
          hosts:
            ceph-openshift-test-1.int.osci.io:
              ansible_host: 172.24.32.34
            ceph-openshift-test-2.int.osci.io:
              ansible_host: 172.24.32.35
            ceph-openshift-test-3.int.osci.io:
              ansible_host: 172.24.32.36
            ceph-openshift-test-4.int.osci.io:
              ansible_host: 172.24.32.37
            ceph-openshift-test-5.int.osci.io:
              ansible_host: 172.24.32.38
            ceph-openshift-test-6.int.osci.io:
              ansible_host: 172.24.32.39

    tenant_operate_first:
      hosts:
        lists.operate-first.cloud:
          ansible_host: 8.43.85.122

    tenant_elekto:
      hosts:
        test.elekto.io:
          ansible_host: 8.43.85.136

    tenant_forgefed:
      hosts:
        pagure-forgefed.osci.io:
          ansible_host: 8.43.85.124

    # these hosts are not managed here but needed for some deployments
    # (mostly used for delegations)
    #
    # if osci_monitoring is set to True (on a per tenant/host basis)
    # Zabbix will be deployed on the tenant host and maintained here
    # thus we need root access and be careful for possible conflits
    # TODO: check if part of our base role is needed for deployment
    #       (like enabled package repositories)
    #
    # if osci_access is True (the default), then our SSH keys will be
    # installed for the root account
    unmanaged:
      vars:
        ansible_user: root
        osci_monitoring: False
      children:
        osci_okd:
        tenant_atomic:
        tenant_gdb:
        tenant_ovirt:
        tenant_pcp:
        tenant_pulp:
        tenant_rdo:
        tenant_python:
        tenant_beaker:
        tenant_heptapod:
        tenant_elekto:
        tenant_forgefed:
      hosts:
        # listed as unmanaged because ostree wreck havoc with our playbooks
        patternfly-forum.osci.io:

    cage_internal_zone:
      hosts:
        osci-web-builder.int.osci.io:
        ovirt-web-builder.int.osci.io:
        pulp-web-builder.int.osci.io:
        rdo-web-builder.int.osci.io:
        koji-web-builder.int.osci.io:
      children:
        ceph_openshift_tests:

    cage_services_zone:
      hosts:
        lucille.srv.osci.io:
        bigaldente.srv.osci.io:

    cage_management_zone:
      children:
        supermicro_microblade_sw:

    catatonic:
      children:
        supermicro_microblade_sw_catatonic:
        supermicro_microblade_blades_catatonic:
      hosts:
        cmm-catatonic.adm.osci.io:
          ansible_host: 172.24.31.4

    network_equipments:
      children:
        supermicro_microblade_sw:

    not_linux:
      hosts:
        cmm-catatonic.adm.osci.io:
      children:
        supermicro_microblade_sw:

    supermicro_microblade_sw:
      children:
        supermicro_microblade_sw_catatonic:

    supermicro_microblade_sw_catatonic:
      hosts:
        switch-a1-catatonic.adm.osci.io:
          ansible_host: 172.24.31.247
        switch-a2-catatonic.adm.osci.io:
          ansible_host: 172.24.31.248

    supermicro_microblade_blades:
      children:
        supermicro_microblade_blades_catatonic:

    supermicro_microblade_blades_catatonic:
      children:
        osci_okd:
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:
        augur.osci.io:
        sandiego.osci.io:
        cauldron.osci.io:
        lucille.srv.osci.io:
        catton.osci.io:
        heptapod.osci.io:
        bigaldente.srv.osci.io:

    virt_hosts:
      hosts:
        spritz.osci.io:
        badcat.osci.io:

    web_builders:
      hosts:
        osci-web-builder.int.osci.io:
        koji-web-builder.int.osci.io:

    dns_servers:
      children:
        dns_servers_ns1:
          hosts:
            polly.osci.io:
        dns_servers_ns2:
          hosts:
            francine.osci.io:
            carla.osci.io:

    gitlab_runners:
      hosts:
        gl-runner-podman-01.osci.io:

    mail_servers:
      children:
        mail_servers_mx1:
          hosts:
            polly.osci.io:
        mail_servers_mx2:
          hosts:
            francine.osci.io:
            carla.osci.io:

    ml_servers:
      hosts:
        lists.opensourceinfra.org:
        lists.minishift.io:
        lists.po4a.org:
        lists.nfs-ganesha.org:
        lists.opendatahub.io:
        lists.podman.io:
        mail.ovirt.org:
        tosw-ng-2019.theopensourceway.org:
        lists.jboss.org:
        lists.shipwright.io:
        lists.chrisproject.org:
        lists.osbusiness.org:
        lists.dogtagpki.org:
        lists.enterpriseneurosystem.org:
        lists.operate-first.cloud:

    ntp_servers:
      hosts:
        spritz.osci.io:
        badcat.osci.io:

    # beware, only 1 server here, since that server is only
    # here to do API call to openshift
    openshift_controller:
      hosts:
        soeru.osci.io:
    # same, please keep it to 1 server
    # need to use a EL 7 host, since
    # iam_policy need python-boto on the ansible version in Fedora and EL7
    # and EL8 do not provides it:
    # https://bugzilla.redhat.com/show_bug.cgi?id=1815847
    aws_controller:
      hosts:
        www.osci.io:

    pxe_servers:
      hosts:
        catton.osci.io:
        www.osci.io:

    sup_servers:
      hosts:
        catton.osci.io:

    zanata_servers:
      hosts:
        translate.zanata.org:

    # for these hosts services will NOT be restarted automagically after yum/dnf-cron updates
    # this is intended as a stopgap, proper per-service configuration exceptions are prefered
    needrestart_noauto:
      hosts: {}

