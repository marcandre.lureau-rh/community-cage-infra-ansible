This list is for your questions, concerns or comments about the Buildah project (https://github.com/containers/buildah).  The list will also be used by the maintainers when there are announcements of interest to the community.  To send an email to the list, simply send an email to buildah@lists.buildah.io with an appropriate subject.  To see the archives or to manage your subscription, go to https://lists.podman.io/archives/list/buildah@lists.buildah.io/.

Buildah is an Open Source project on GitHub that provides a command line tool which can be used to create a container image or a working container.  The container images can be built from 'scratch', by using Buildah command line instructions, or by using an industry standard Dockerfile.  Buildah can create and use both the Open Containers Initiative (OCI) or the traditional upstream Docker container image format.  A container root file system can be mounted for manipulation and then the updated contents can be used to create a new image.  Buildah also offers the ability to remove and rename containers and container images.

Buildah has a web site (https://www.buildah.io) devoted to blogs and announcements for the project along with tutorials and slides from talks that the maintainers have made at a variety of conferences.  In addition to the website and mailing lists, many of the maintainers monitor the #buildah channel on the freenode IRC network.

Buildah has a sister project, Podman, which manages everything in the Kubernetes universe such a pods, containers, container images and container volumes.  Podman has its own website (https://www.podman.io) that you can check out for more information.

Discussions about issues with Buildah on this list are welcomed.   If you are using a distribution package of Buildah, be sure to file a bug against the distribution when you have problems.  Otherwise, if you are using upstream code from our GitHub repository,  you'll need to raise an issue in GitHub (https://github.com/containers/buildah/issues).

We're happy to have you join our community and look forward to your contributions!
