#!/bin/sh
set -e

# ensure PATH is properly set
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

MNT_PATH=/mnt/backup

for vglvtype in "$@"; do
  vg=$(echo ${vglvtype} | cut -d/ -f1)
  lv=$(echo ${vglvtype} | cut -d/ -f2)
  type=$(echo ${vglvtype} | cut -d/ -f3)

  dev=/dev/${vg}/${lv}
  if [ ! -e $dev ]; then
    echo "VG/LV '${vg}/${lv}' is missing" >&2
    continue
  fi

  mnt=${MNT_PATH}/${vg}/${lv}
  mkdir -p $mnt

  if mountpoint -q $mnt; then
    umount $mnt
  fi

  dev_snap=/dev/${vg}/${lv}_snap
  if [ -e $dev_snap ]; then
    lvremove -y $dev_snap >/dev/null
  fi

  # lock tables on all databases and flush buffers to have a coherent state on disk
  # TODO: use BACKUP STAGE instead if mariaDB >=10.4.1
  if [ "${type}" == "mysql" ]; then
    coproc mysql --unbuffered 2>&1

    if ! ps -p ${COPROC_PID} >/dev/null; then
      echo "could not table lock MySQL for VG/LV '${vg}/${lv}'" >&2
      continue
    fi

    echo "FLUSH TABLES WITH READ LOCK;" >&${COPROC[1]}
  fi

  lvcreate -s -L 1G -n ${lv}_snap $dev >/dev/null

  if [ "${type}" == "mysql" ]; then
    echo "UNLOCK TABLES;" >&${COPROC[1]}

    # no semicolon or it complains about the syntax
    echo "exit" >&${COPROC[1]}
    wait ${COPROC_PID}
  fi

  mount -o nouuid $dev_snap $mnt
done

